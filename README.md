# Emacs Dot Files
This represents my personal `.emacs.d` setup.

## Setup
The `init.el` only does a couple things:
- Add addtional package archives and initialize them
- Install `diminish` and `use-package`, everything is built off of them
- Add the `modules** directory and all subdirectories to the list of package load paths
- Load all the modules

**Note:** Each module root `provides` itself. This means when loaded, it adds itself to the list of loaded features. When `require` is called, it only loads the module if it isn't in the list of loaded features.

All packages are organized into modules. The root file of each module has the file name of `<module>-mod.el`. It loads all the other parts.
All packages installed from a package repository are done so using `use-package`, most of them using hooks or bindings that defer loading them until they are used.

Modules and their purpose:
- `checkers` - Spelling, grammar, linter, and other text checkers.
- `completion` - Completion at point, Emacs interface completion (jumping, commands, etc), and code snippets.
- `core` - Configuring core Emacs features, mostly settings for Emacs built-ins.
- `lang` - Setup for various programming languages, tools, and formats.
- `projects` - All things related to project managment.
- `ui` - Various packages and configuration for changing the look and feel of Emacs.
- `vc` - Packages related to version control integration.

The last folder is the `banners` folder. This holds text and image banners that get installed to the `dashboards` banner location.
