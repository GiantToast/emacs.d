;;; vc-magit.el --- Installs and Configures Magit
;;; Commentary:
;;; Code:
(use-package transient
  :ensure t)

(use-package magit
  :ensure t
  :bind ("C-x g" . magit-status))
;;; vc-magit.el ends here
