;;; vc-mod.el --- Modules for Working with Version Control
;;; Commentary:
;; This module installs and configures packages to make working with vc better.

;;; Code:
(load "vc-magit.el")

(provide 'vc-mod)
;;; vc-mod.el ends here