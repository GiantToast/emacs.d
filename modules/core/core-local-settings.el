;;; core-local-settings.el --- Configures Emacs with any local settings
;;; Commentary:
;;; Code:
(set-face-attribute 'default nil :height 180)
;;; core-local-settings.el ends here
