;;; core-signatures.el --- Configures Checking Signatures and Certs
;;; Commentary:
;;; Code:
;;(setq package-check-signature nil)

;; (defvar gnutls-trustfiles)
;; (with-eval-after-load 'gnutls
;;   (add-to-list 'gnutls-trustfiles "/usr/local/etc/openssl@1.1/cert.pem"))
;;; core-signatures.el ends here
