;;; core-mod.el --- Setup Core Emacs Features
;;; Commentary:
;; This module changes some core behaviors of Emacs.
;; Things like backup files, autosave files, customize variables, etc.

;;; Code:
(load "core-package-management-elpaca.el")
(load "core-signatures.el")
(load "core-path.el")
(load "core-customize.el")
(load "core-autosaves.el")
(load "core-backups.el")
(load "core-whitespace.el")
(load "core-local-settings.el" t)

(global-set-key (kbd "C-x C-b") 'ibuffer)
(setq default-directory "~")

(setq-default indent-tabs-mode nil)

(provide 'core-mod)
;;; core-mod.el ends here
