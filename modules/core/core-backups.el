;;; core-backups.el --- Configures Emacs Backup File Settings
;;; Commentary:
;;; Code:
(defvar backup-dir)
(setq backup-dir (concat user-emacs-directory (convert-standard-filename "backups/")))
(when (not (file-directory-p backup-dir)) (make-directory backup-dir t))
(setq backup-by-copying t
      backup-directory-alist `((".*" . ,backup-dir)))
;;; core-backups.el ends here
