;;; core-customize.el --- Configures Emacs Customize Files Settings
;;; Commentary:
;;; Code:
(setq custom-file (concat user-emacs-directory (convert-standard-filename "custom.el")))
(load custom-file 'noerror)
;;; core-customize.el ends here
