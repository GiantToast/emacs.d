;;; core-autosaves.el --- Configures Emacs Autosave Settings
;;; Commentary:
;;; Code:
(defvar autosave-dir)
(setq autosave-dir (concat user-emacs-directory (convert-standard-filename "autosaves/")))
(when (not (file-directory-p autosave-dir)) (make-directory autosave-dir t))
(setq auto-save-file-name-transforms `((".*" ,autosave-dir t)))
;;; core-autosaves.el ends here
