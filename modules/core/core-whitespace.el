;;; core-whitespace.el --- Cleanup whitespace on save
;;; Commentary:
;;; Code:
(add-hook 'write-file-functions 'delete-trailing-whitespace)
;;; core-whitespace.el ends here
