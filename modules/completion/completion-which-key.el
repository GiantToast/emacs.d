;;; completion-which-key.el --- Installs and Configures Which Key
;;; Commentary:
;;; Code:
(use-package which-key
  :ensure t
  :config (which-key-mode))
;;; completion-which-key.el ends here
