;;; completion-ivy.el --- Installs and Configures ivy, swiper, and counsel
;;; Commentary:
;; Counsel also brings in ivy and swiper

;;; Code:
(use-package counsel
  :ensure t
  :init (ivy-mode 1)
  :bind (("M-x" . counsel-M-x)
         ("C-s" . swiper)
         ("C-x C-f" . counsel-find-file)
         ("C-x C-r" . counsel-recentf)
         ("C-h f" . counsel-describe-function)
         ("C-h v" . counsel-describe-variable)
         ("C-c C-r" . ivy-resume))
  :config (setq ivy-count-format ""))
;;; completion-ivy.el ends here
