;;; completion-mod.el --- Packages Related to Autocompletion
;;; Commentary:
;; The module installs and configures packages related to autocompletion
;; Autocompletion can mean language specific, or Emacs features

;;; Code:
(load "completion-ivy.el")
(load "completion-avy.el")
(load "completion-ace-window.el")
(load "completion-company.el")
(load "completion-which-key.el")
(load "completion-yasnippet.el")
(load "completion-parens.el")
(load "completion-cursors.el")

(provide 'completion-mod)
;;; completion-mod.el ends here
