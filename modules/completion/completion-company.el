;;; completion-company.el --- Installs and Configures Company
;;; Commentary:
;;; Code:
(use-package company
  :ensure t
  :config
  (setq company-minimum-prefix-length 1
        company-idle-delay 0.0)
  (company-mode))
;;; completion-company.el ends here
