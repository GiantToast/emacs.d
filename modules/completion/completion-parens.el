;;; completion-parens.el --- Installs and Configures SmartParens
;;; Commentary:
;;; Code:
(use-package smartparens
  :ensure t
  :config
  (require 'smartparens-config)
  (smartparens-global-mode))
;;; completion-parens.el ends here
