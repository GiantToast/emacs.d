;;; completion-ace-window.el --- Installs and Configures Ace Window
;;; Commentary:
;;; Code:
(use-package ace-window
  :ensure t
  :bind ("C-x o" . ace-window))
;;; completion-ace-window.el ends here
