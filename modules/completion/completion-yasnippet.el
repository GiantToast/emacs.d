;;; completion-yasnippet.el --- Installs and Configures Yasnippet
;;; Commentary:
;;; Code:
(use-package yasnippet
  :ensure t
  :init
  (yas-global-mode 1))
;;; completion-yasnippet.el ends here
