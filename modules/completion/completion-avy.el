;;; completion-avy.el --- Installs and Configures Avy
;;; Commentary:
;;; Code:
(use-package avy
  :ensure t
  :bind (("C-." . avy-goto-word-1)
         ("C-:" . avy-goto-char)
         ("M-g g" . avy-goto-line)))
;;; completion-avy.el ends here
