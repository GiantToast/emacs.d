;;; completion-cursors.el --- Installs and Configures Multiple Cursors
;;; Commentary:
;;; Code:
(use-package multiple-cursors
  :ensure t
  :bind (("C->" . mc/mark-next-like-this)
	 ("C-<" . mc/mark-previous-like-this)
	 ("C-c C->" . mc/mark-all-like-this)
	 ("C-c C-<" . mc/mark-all-like-this)
	 ("C-S-c C-S-c" . mc/edit-lines)))
;;; completion-cursors.el ends here
