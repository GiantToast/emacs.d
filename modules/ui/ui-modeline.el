;;; ui-modeline.el --- Installs and Configures Modeline
;;; Commentary:
;;; Code:
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode t)
  :config
  (setq doom-modeline-buffer-file-name-style 'auto))
;;; ui-modeline.el ends here
