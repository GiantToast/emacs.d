;;; ui-treemacs.el --- Install and Configures Treemacs
;;; Commentary:
;;; Code:
(use-package treemacs
  :ensure t
  :bind (("M-0" . treemacs-select-window)
	 ("M-p" . treemacs-display-current-project-exclusively)))

(use-package treemacs-projectile
  :after (treemacs projectile)
  :ensure t)

(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t)
;;; ui-treemacs.el ends here
