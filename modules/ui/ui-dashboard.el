;;; ui-dashboard.el --- Installs and Configures Dashboard Packages
;;; Commentary:
;;; Code:
(defvar user-banners-dir)
(setq user-banners-dir (concat user-emacs-directory (convert-standard-filename "banners/")))

(defun install-banners ()
  "Copy all files under under banners directory to dashboard banners directory."
  (when (boundp 'dashboard-banners-directory)
    (copy-directory user-banners-dir dashboard-banners-directory nil nil t)))

(use-package page-break-lines
  :ensure t)

(use-package dashboard
  :ensure t
  :after (page-break-lines)
  :config
  (install-banners)
  (setq dashboard-banner-logo-title "")
  (setq dashboard-startup-banner 4)
  (setq dashboard-center-content t)
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  (setq dashboard-items '((recents . 5)
                          (projects . 5)))
  (setq dashboard-page-separator "\n\n")
  (setq dashboard-set-footer nil)
  (setq dashboard-set-navigator t)
  (dashboard-setup-startup-hook))
;;; ui-dashboard.el ends here
