;;; ui-interface.el --- Configures Build-In Emacs Interface Settings
;;; Commentary:
;;; Code:
(menu-bar-mode -1)
(tool-bar-mode -1)
(toggle-scroll-bar -1)
(global-display-line-numbers-mode)

(setq inhibit-startup-message t)
(setq initial-scratch-message nil)
(setq ring-bell-function 'ignore)
;;; ui-interface.el ends here
