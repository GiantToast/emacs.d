;;; ui-all-the-icons.el -- Installs All the Icons
;;; Commentary:
;;; Code:
(use-package all-the-icons
  :ensure t)
;;; ui-all-the-icons.el ends here
