;;; ui-mod.el --- Module to Configure Emacs Aesthetics
;;; Commentary:
;; This module is for installing and configuring all things related to looks.

;;; Code:
(load "ui-interface.el")
(load "ui-all-the-icons.el")
(load "ui-treemacs.el")
(load "ui-theme.el")
(load "ui-modeline.el")
(load "ui-dashboard.el")

(provide 'ui-mod)
;;; ui-mod.el ends here
