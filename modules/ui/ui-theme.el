;;; ui-theme.el --- Installs and Configures Themes
;;; Commentary:
;;; Code:
(use-package doom-themes
  :ensure t
  :config
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t)
  (doom-themes-treemacs-config)
  (doom-themes-visual-bell-config)
  (load-theme 'doom-one t))
;;; ui-theme.el ends here
