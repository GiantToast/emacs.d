;;; projects-mod.el --- Module for Managing Projects
;;; Commentary:
;; This module installs and configures packages that deal with project management

;;; Code:
(load "projects-projectile.el")
(load "projects-org-mode.el")
(load "projects-org-roam.el")

(provide 'projects-mod)
;;; projects-mod.el ends here
