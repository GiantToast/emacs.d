;;; projects-org-roam.el --- Install and Configure Org Roam Packages
;;; Commentary:
;;; Code:
(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory (file-truename "~/org-roam"))
  (org-roam-completion-everywhere t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         :map org-mode-map
         ("C-c n c" . completion-at-point))
  :config
  (org-roam-db-autosync-mode))
;;; projects-org-roam.el ends here
