;;; projects-org-mode.el --- Configure Org Mode
;;; Commentary:
;;; Code:
(use-package org
  :ensure t
  :config
  (setq org-hide-leading-stars 1)
  (setq org-return-follows-link 1))

(require 'ox-md)
;;; projects-org-mode.el ends here
