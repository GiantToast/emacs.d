;;; lang-python.el --- Installs and Configures Python Packages
;;; Commentary:
;;; Code:
(use-package conda
  :init
  (setq conda-anaconda-home (expand-file-name "~/anaconda3"))
  (setq conda-env-home-directory (expand-file-name "~/anaconda3"))
  :ensure t)

(use-package ein
  :ensure t)
;;; lang-python.el ends here
