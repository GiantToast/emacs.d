;;; lang-web.el --- Installs and Configures Web Development Packages
;;; Commentary:
;;; Code:
(use-package web-mode
  :ensure t
  :mode (("\\.html\\'" . web-mode)
         ("\\.ts\\'" . web-mode)
         ("\\.tsx\\'" . web-mode)
         ("\\.js\\'" . web-mode)
         ("\\.jsx\\'" . web-mode)
         ("\\.svelte\\'" . web-mode)
         ("\\.vue\\'" . web-mode)
         ("\\.vuex\\'" . web-mode))
  :custom
  (web-mode-markup-indent-offset 2)
  (web-mode-css-indent-offset 4)
  (web-mode-code-indent-offset 4))
;;; lang-web.el ends here
