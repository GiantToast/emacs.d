;;; lang-hcl.el --- Installs and Configures Hashicorp Modes
;;; Commentary:
;;; Code:
(use-package hcl-mode
  :ensure t)

(defvar terraform-auto-format-on-save)

(use-package terraform-mode
  :ensure t
  :hook (terraform-mode . terraform-format-on-save-mode)
  :config
  (setq terraform-auto-format-on-save t))
;;; lang-hcl.el ends here
