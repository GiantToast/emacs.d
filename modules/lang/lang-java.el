;;; lang-java.el --- Install and Configures Java Packages
;;; Commentary:
;;; Code:
(use-package lsp-java
  :ensure t
  :hook (java-mode . lsp))
;;; lang-java.el ends here
