;;; lang-godot.el --- Installs and Configures Godot Packages
;;; Commentary:
;;; Code:
(use-package gdscript-mode
  :ensure t
  :hook (gdscript-mode . lsp)
  :custom (lsp-gdscript-port 6005))
;;; lang-godot.el ends here
