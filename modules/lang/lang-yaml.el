;;; lang-yaml.el --- Installs and Configures Yaml Packages
;;; Commentary:
;;; Code:
(use-package yaml-mode
  :ensure t)
;;; lang-yaml.el ends here
