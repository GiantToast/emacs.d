;;; lang-clojure.el --- Installs and Configures Clojure Packages
;;; Commentary:
;;; Code:
(use-package clojure-mode
  :ensure t
  :hook ((clojure-mode . lsp)
         (clojurec-mode . lsp)
         (clojurescript-mode . lsp)))

(use-package cider
  :ensure t)
;;; lang-clojure.el ends here
