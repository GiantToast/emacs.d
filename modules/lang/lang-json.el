;;; lang-json.el --- Installs and Configures JSON Mode
;;; Commentary:
;;; Code:
(use-package json-mode
  :ensure t)
;;; lang-json.el ends here
