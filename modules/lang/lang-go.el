;;; lang-go.el --- Installs and Configures Go Packages
;;; Commentary:
;;; Code:
(use-package go-mode
  :ensure t
  :hook (go-mode . lsp))
;;; lang-go.el ends here
