;;; lang-scala.el --- Installs and Configures Scala Packages
;;; Commentary:
;;; Code:
(defvar lsp-prefer-flymake)

;; (defun scala-format-hook ()
;;   "Format scala file using the language server."
;;   (when (and (eq major-mode 'scala-mode) (fboundp 'lsp-format-buffer))
;;     (lsp-format-buffer)))

(use-package lsp-metals
  :ensure t
  :hook (scala-mode . lsp))

;; (use-package scala-mode
;;   :ensure t
;;   :mode "\\.s\\(cala\\|bt\\)$"
;;   :hook (scala-mode . lsp)
;;   :config
;;   (setq lsp-prefer-flymake nil)
;;   (setq lsp-enable-indentation nil)
;;   (add-hook 'before-save-hook #'scala-format-hook))

;; (use-package sbt-mode
;;   :ensure t
;;   :commands sbt-start sbt-command
;;   :config
;;   ;; allows using SPACE when in the minibuffer
;;   (substitute-key-definition
;;    'minibuffer-complete-word
;;    'self-insert-command
;;    minibuffer-local-completion-map)
;;    ;; sbt-supershell kills sbt-mode:  https://github.com/hvesalai/emacs-sbt-mode/issues/152
;;   (setq sbt:program-options '("-Dsbt.supershell=false")))

;; Scala 3 Workarounds
;; (defun is-scala3-project ()
;;   "Check if the current project is using scala3. Load the build.sbt file for the project and serach for the scalaVersion."
;;   (projectile-with-default-dir (projectile-project-root)
;;     (when (file-exists-p "build.sbt")
;;       (with-temp-buffer
;;         (insert-file-contents "build.sbt")
;;         (search-forward "scalaVersion := \"3" nil t)))))

;; (defun with-disable-for-scala3 (orig-scala-mode-map:add-self-insert-hooks &rest arguments)
;;     "When using scala3 skip adding indention hooks."
;;     (unless (is-scala3-project)
;;       (apply orig-scala-mode-map:add-self-insert-hooks arguments)))

;; (advice-add #'scala-mode-map:add-self-insert-hooks :around #'with-disable-for-scala3)

;; (defun disable-scala-indent ()
;;   "In scala 3 indent line does not work as expected due to whitespace grammar."
;;   (when (is-scala3-project)
;;     (setq indent-line-function 'indent-relative-maybe)))

;; (add-hook 'scala-mode-hook #'disable-scala-indent)
;;; lang-scala.el ends here
