;;; lang-mod.el --- Programming Language Integration
;;; Commentary:
;; This module installs and configures packages for working with various programming languages and tools.

;;; Code:
(load "lang-lsp.el")
(load "lang-scala.el")
(load "lang-web.el")
(load "lang-rest.el")
(load "lang-json.el")
(load "lang-hcl.el")
(load "lang-clojure.el")
(load "lang-java.el")
(load "lang-vue.el")
(load "lang-alda.el")
(load "lang-yaml.el")
(load "lang-go.el")
(load "lang-python.el")
(load "lang-rust.el")
(load "lang-godot.el")

(provide 'lang-mod)
;;; lang-mod.el ends here
