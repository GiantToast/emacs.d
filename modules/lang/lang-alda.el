;;; lang-alda.el --- Installs and Configures Alda Development Packages
;;; Commentary:
;;; Code:
(use-package alda-mode
  :ensure t)
;;; lang-alda.el ends here
