;;; lang-lsp.el --- Installs and Configures LSP Mode
;;; Commentary:
;;; Code:
(defvar lsp-keymap-prefix)
(defvar lsp-prefer-flymake)

(setq lsp-keymap-prefix "C-c l")

(use-package lsp-mode
  :ensure t
  :hook ((lsp-mode . lsp-lens-mode)
         (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp
  :config
  (setq lsp-prefer-flymake nil))

(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode
  :config
  (setq lsp-headerline-breadcrumb-enable nil))

(use-package lsp-ivy
  :ensure t
  :commands lsp-ivy-workspace-symbol)
;;; lang-lsp.el ends here
