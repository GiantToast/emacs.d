;;; lang-vue.el --- Installs and Configures Vue Development Packages
;;; Commentary:
;;; Code:
(use-package vue-html-mode
  :ensure t)

(use-package vue-mode
  :ensure t
  :mode ("\\.vue\\'" . vue-mode)
  :hook (vue-mode . lsp))
;;; lang-vue.el ends here
