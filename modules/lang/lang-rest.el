;;; lang-rest.el --- Installs and Configures RestClient
;;; Commentary:
;;; Code:
(use-package restclient
  :ensure t
  :mode ("\\.rest\\'" . restclient-mode))

(use-package company-restclient
  :ensure t
  :config
  (add-to-list 'company-backends 'company-restclient))
;;; lang-rest.el ends here
