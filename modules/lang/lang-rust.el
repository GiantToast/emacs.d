;;; lang-rust.el --- Installs and Configures Rust Packages
;;; Commentary:
;;; Code:
(use-package rustic
  :ensure t
  :custom
  (rustic-analyzer-command '("rustup" "run" "stable" "rust-analyzer")))
;;; lang-rust.el ends here
