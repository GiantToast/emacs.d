;;; ai-gptel.el ---  GPTEL Integration
;;; Commentary:
;; Installs and configures gptel

;;; Code:
;; Built on GPTEL, makes interaction in code easier
(use-package elysium
  :ensure t)

;; Plumbing for talking with a ton of different AI models
(use-package gptel
  :ensure t)
;;; ai-gptel.el ends here
