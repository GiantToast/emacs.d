;;; ai-mod.el --- AI Stuff
;;; Commentary:
;; This module installs and configures packages for working with AI.

;;; Code:
(load "ai-gptel.el")
(load "ai-codeium.el")

(provide 'ai-mod)
;;; ai-mod.el ends here
