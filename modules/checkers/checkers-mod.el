;;; checkers-mod.el --- Packages for Checking Things
;;; Commentary:
;; The module installs and configures linters, spelling, and grammar checkers

;;; Code:
(load "checkers-flycheck")

(provide 'checkers-mod)
;;; checkers-mod.el ends here