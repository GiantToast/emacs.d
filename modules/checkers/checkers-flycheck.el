;;; checkers-flycheck.el --- Installs and Configures Flycheck
;;; Commentary:
;;; Code:
(use-package flycheck
  :ensure t
  :config (global-flycheck-mode))
;;; checkers-flycheck.el ends here
