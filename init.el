;;; init.el --- Emacs Setup Entry Point
;;; Commentary:
;;; Code:
;; Disable default package management:
(setq package-enable-at-startup nil)

;; Prevent showing warnings buffer for minor issues
(setq warning-minimum-level :error)

;; Add Modules and Subdirs to Load Path
(defvar modules-dir)
(setq modules-dir (concat user-emacs-directory (convert-standard-filename "modules/")))
(let ((default-directory modules-dir))
  (normal-top-level-add-subdirs-to-load-path))

;; Lets Flycheck know about the loaded paths
(defvar flycheck-emacs-lisp-load-path)
(setq flycheck-emacs-lisp-load-path 'inherit)

;; Load Modules
(require 'core-mod)
(require 'ui-mod)
(require 'checkers-mod)
(require 'completion-mod)
(require 'ai-mod)
(require 'vc-mod)
(require 'projects-mod)
(require 'lang-mod)
;;; init.el ends here
